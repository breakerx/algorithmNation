package cn.xsshome.algorithmNation.sample;

import cn.xsshome.algorithmNation.util.SM4EnDecryption;
import cn.xsshome.algorithmNation.util.Util4Hex;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

/**
 * 国密SM4加解密调用示例代码
 * @author 小帅丶 
 */
public class TestSM4_xl {
	//明文数据
    protected static final byte[] SRC_DATA = "13569602110".getBytes();
	public static void main(String[] args) throws Exception {
		BASE64Encoder encoder = new BASE64Encoder();
		BASE64Decoder decoder = new BASE64Decoder();

		System.out.println("数研所字段解密：" + decsm4("SWXO/InzvruUzSWsQpjSlQ=="));
		System.out.println("decode结果" + Util4Hex.bytesToHexString(decoder.decodeBuffer("SWXO/InzvruUzSWsQpjSlQ==")));

		byte[] sm4key = Util4Hex.hexStringToBytes("36D831B7555289C53AE8716BB67F2A2D");
		System.out.println("SM4密钥:"+Util4Hex.bytesToHexString(sm4key));
		byte[] cipherText = null;
        byte[] decryptedData = null;
        /*********************ECB加解密*************************/
        cipherText = SM4EnDecryption.encrypt_Ecb_Padding(sm4key, SRC_DATA);
        System.out.println("SM4 ECB Padding 加密结果16进制:\n" + Util4Hex.bytesToHexString(cipherText));
        System.out.println("SM4 ECB Padding 加密结果:\n" + encoder.encode(cipherText));

        decryptedData = SM4EnDecryption.decrypt_Ecb_Padding(sm4key, cipherText);
        System.out.println("SM4 ECB Padding 解密结果:\n" + new String(decryptedData));

	}
	public static String decsm4(String encstr){
		BASE64Decoder decoder = new BASE64Decoder();
		String result = null;
		try {
			String enstr64 = Util4Hex.bytesToHexString(decoder.decodeBuffer("SWXO/InzvruUzSWsQpjSlQ=="));
			byte[] sm4key = Util4Hex.hexStringToBytes("36D831B7555289C53AE8716BB67F2A2D");
			byte[] cipherText = decoder.decodeBuffer(encstr);
			byte[] decryptedData = SM4EnDecryption.decrypt_Ecb_Padding(sm4key, cipherText);
			result = new String(decryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
